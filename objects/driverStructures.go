package structures

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

// RegisterSession - TODO:
type RegisterSession struct {
	tableName  struct{}  `sql:"drv_reg_sessions"`
	SSID       string    `json:"ssid" sql:",pk" description:"Session ID"`
	DeviceID   string    `json:"device_id"`
	Phone      string    `json:"phone"`
	Code       int       `json:"code"`
	Expiration time.Time `json:"expiration_time"`
	UsedAt     time.Time `json:"used_time"`
	CreatedAt  time.Time `sql:"default:now()" json:"created_at"`
}

// DriverRegistrRequest - структура запроса на регистрацию
type DriverRegistrRequest struct {
	DeviceID string `json:"device_id"`
	Phone    string `json:"phone"`
}

// NewRegResponseStruct - REST response structs
type NewRegResponseStruct struct {
	Code            int    `json:"code"`
	Msg             string `json:"message"`
	NextRequestTime int64  `json:"next_request_time"`
}

// VerificationCode - структура для получения кода
type VerificationCode struct {
	DeviceID string `json:"device_id"`
	Code     int    `json:"code"`
}

// TokenResponse - структура ответа с токеным
type TokenResponse struct {
	DriverID               int    `json:"driver_id"`
	Token                  string `json:"token"`
	DriverUUID             string `json:"driver_uuid"`
	RefreshToken           string `json:"refresh_token"`
	RefreshTokenExpiration int64  `json:"refresh_expiration"`
}

// TokenClaim содержание самого токена
type TokenClaim struct {
	DriverID   int    `json:"driver_id"`
	DriverUUID string `json:"driver_uuid"`
	DeviceID   string `json:"device_id"`
	Phone      string `json:"phone"`
	jwt.StandardClaims
}

// RefreshRequest структура запроса refrash механизма
type RefreshRequest struct {
	RefreshToken string `json:"refresh"`
}

// JWTSessions godoc
type JWTSessions struct {
	tableName           struct{}  `sql:"drv_jwt_sessions"`
	ID                  int       `json:"id" sql:",pk"`
	DriverID            int       `json:"driver_id"`
	DriverUUID          string    `json:"driver_uuid"`
	RefreshToken        string    `json:"refresh_token" description:"Refresh token"`
	RefreshTokenUsed    time.Time `json:"refresh_token_used" description:"Refresh token used date"`
	RefreshTokenExpired time.Time `json:"refrash_expired" description:"Token expiration date"`
	CreatedAt           time.Time `sql:"default:now()" json:"created_at" `
}
