package structures

import "time"

// LoginResponse responsed when requesting token
type LoginResponse struct {
	UserID                 int       `json:"user_id"`
	Token                  string    `json:"token"`
	RefreshToken           string    `json:"refresh_token"`
	RefreshTokenExpiration time.Time `json:"refresh_expiration"`
}

// OrderCRM - TODO
type OrderCRM struct {
	tableName       struct{}  `sql:"crm_orders"`
	Order                     //structures.Order
	ID              int       `json:"id" sql:",pk"`
	ClUUID          string    `json:"client_uuid"`
	SrUUID          string    `json:"service_uuid"`
	CallbackPhone   string    `json:"callback_phone"`
	FeaturesUUIDS   []string  `json:"features_uuids"`
	StartUserUUID   string    `json:"-"`
	LastUserUUID    string    `json:"-"`
	Driver          Driver    `json:"driver"` //Driver structures.Driver `json:"driver"`
	FinishUserUUID  string    `json:"-"`
	AppointmentTime time.Time `json:"-" description:"Время назначения авто на заказ"`
	CompleteTime    time.Time `json:"-" description:"Врем завершения заказа"`
	PickupTime      time.Time `json:"-" description:"Прибытие авто"`
	OwUUID          string    `json:"owner_uuid"`
	CreatedAt       time.Time `sql:"default:now()" json:"created_at" description:"Дата создания"`
	UpdatedAt       time.Time `json:"updated_at" `
	Deleted         bool      `json:"-" sql:"default:false"`
	OrderState      string    `json:"state_name" `
	StateTitle      string    `json:"state_title" sql:"-"`
	// OrderStateUUID  string    `json:"order_state_uuid" `
}

// Order базовая структура заказа
type Order struct {
	UUID     string    `json:"uuid" sql:",pk"`
	Comment  string    `json:"comment"`
	Routes   []Route   `json:"routes"`
	Features []Feature `json:"features"`
	Tariff   Tariff    `json:"tariff,omitempty"`
	Service  Service   `json:"service"`
	Owner    Owner     `json:"owner"`
	Client   Client    `json:"client"`
}

// CreaterOrder - структура для передачи в функцию создания заказа
type CreaterOrder struct {
	Client        Client    `json:"client"`
	CallbackPhone string    `json:"callback_phone"`
	Comment       string    `json:"comment"`
	Features      []Feature `json:"features"`
	Routes        []Route   `json:"routes"`
	OwnerUUID     string    `json:"owner_uuid"`
	ServiceUUID   string    `json:"service_uuid"`
}

// OptionsForOrder структура для опций заказа
type OptionsForOrder struct {
	Owners   []Owner
	Services []Service
	Routes   []Route
	Features []Feature
}

// Route точка назначения в заказе
type Route struct {
	UnrestrictedValue string  `json:"unrestricted_value"  description:"полный адрес"`
	Value             string  `json:"value"  description:"короткий адрес"`
	Country           string  `json:"country"  description:"Страна"`
	Region            string  `json:"region"  description:"Регион"`
	RegionType        string  `json:"region_type"  description:"Тип региона"`
	City              string  `json:"city"  description:"Город"`
	CityType          string  `json:"city_type"  description:"Тип города"`
	Street            string  `json:"street"  description:"Улица"`
	StreetType        string  `json:"street_type"  description:"Тип улицы"`
	StreetWithType    string  `json:"street_with_type"  description:"Улица с типом"`
	House             string  `json:"house"  description:"Дом"`
	OutOfTown         bool    `json:"out_of_town"  description:"Загород ли"`
	HouseType         string  `json:"house_type"  description:"Тип дома"`
	AccuracyLevel     int     `json:"accuracy_level"  description:"Уровень детализации адреса"`
	Radius            int     `json:"radius"  description:"Радиус"`
	Lat               float32 `json:"lat"  description:"Широта"`
	Lon               float32 `json:"lon"  description:"Долгота"`
}

// Tariff Не является справочником! Тариф обрабатывается отдельным сервисом
type Tariff struct {
	Name        string       `json:"name"`
	TotalPrice  int          `json:"total_price"`
	Currency    string       `json:"currency"`
	PaymentType string       `json:"payment_type"`
	Items       []TarrifItem `json:"items"`
}

// TarrifItem структура элемента тарифа
type TarrifItem struct {
	Name  string `json:"name"`
	Price int    `json:"price"`
}

// Service структура услуги
type Service struct {
	UUID       string  `json:"uuid"`
	Name       string  `json:"name"`
	PriceCoeff float32 `json:"price_coefficient"`
	Freight    *bool   `json:"freight"`
	Comment    string  `json:"comment"`
	Tag        string  `json:"tag"`
}

// Owner - order owner structure
type Owner struct {
	UUID    string `json:"uuid"`
	Name    string `json:"name" description:"Название таксопарка"`
	Comment string `json:"comment" description:"Комментарий"`
}

// Client клиенты
type Client struct {
	UUID      string   `json:"uuid"`
	Name      string   `json:"name"`
	Karma     int      `json:"karma"`
	MainPhone string   `json:"main_phone"`
	Phones    []string `json:"phones"`
	Comment   string   `json:"comment"`
}

// Feature - фичи заказа (кресло, багажни и т.д.)
type Feature struct {
	UUID    string `json:"uuid"`
	Name    string `json:"name" description:"Название услуги" required:"true"`
	Comment string `json:"comment" description:"Комментарий"`
	Price   int    `json:"price" description:"Цена"`
}

// Driver водитель. Пока все в одной структуре. AppID решил убрать, он нужен только в DriversApp
type Driver struct {
	UUID      string   `json:"uuid"`
	Name      string   `json:"name" required:"true"`
	Phone     string   `json:"phone" description:"Телефон" required:"true"`
	Comment   string   `json:"comment" description:"Comment"`
	Status    string   `json:"state_name" sql:"status" description:"статус водителя"`
	Car       string   `json:"car" description:"Машина"`
	Karma     int      `json:"karma" description:"Рейтинг"`
	Color     string   `json:"color"`
	Tag       []string `json:"tag" sql:",type:string[]"`
	RegNumber string   `json:"reg_number" description:"Номера машины"`
}
