package endpoints

import (

	// cfg "taxiTest/configs"
	// obj "taxiTest/objects"

	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	cfg "taxiTest/configs"
	"taxiTest/myrabbit"

	//obj "taxiTest/objects"
	obj "taxiTest/objects"

	// jwt "github.com/dgrijalva/jwt-go"

	"github.com/Sirupsen/logrus"
)

// TokenUUID - объект водителя прошедшего верификацию (система)
// привязка к токену id воителя
type TokenUUID struct {
	Token         string
	RabbitInst    myrabbit.MyRabbit
	DriverUUID    string
	State         string
	LastLatitude  float64
	LastLongitude float64
}

// ListTokenUUID - список водителей прошедших верификацию
var ListTokenUUID []TokenUUID

// DoGetSMSVerificationNumber - (http://192.168.0.103:6003/api/v2/auth/new)
func DoGetSMSVerificationNumber(deviceID string, phone string) (obj.NewRegResponseStruct, error) {
	client := &http.Client{}

	var reqInst obj.DriverRegistrRequest
	reqInst.DeviceID = deviceID
	reqInst.Phone = phone

	data, _ := json.Marshal(reqInst)
	logrus.Warn(string(data))
	r := bytes.NewReader(data)
	req, err := http.NewRequest(
		"POST",
		cfg.DriverAddress+cfg.DriverEndPoints["authorivation"],
		r,
	)
	if err != nil {
		fmt.Println(err)
		return obj.NewRegResponseStruct{}, err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return obj.NewRegResponseStruct{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst obj.NewRegResponseStruct
	decoder.Decode(&inst)

	logrus.Debugf("Code = [%v]", inst.Code)
	logrus.Debugf("Message = [%s]", inst.Msg)

	return inst, nil
}

// SendVerificationNumberReq - структура запроса DoSendVerificationNumber
// type SendVerificationNumberReq struct {
// 	DeviceID string `json:"device_id"`
// 	Code     int    `json:"code"`
// }

// SendVerificationNumberResp - структура ответa DoSendVerificationNumber
// type SendVerificationNumberResp struct {
// 	DriverID          int    `json:"driver_id"`
// 	Token             string `json:"token"`
// 	DriverUUID        string `json:"driver_uuid"`
// 	RefreshToken      string `json:"refresh_token"`
// 	RefreshExpiration int    `json:"refresh_expiration"`
// }

// DoSendVerificationNumber - (http://192.168.0.103:6003/api/v2/auth/verification)
func DoSendVerificationNumber(deviceID string, code int) (obj.TokenResponse, error) {
	client := &http.Client{}

	var svnReq obj.VerificationCode
	svnReq.DeviceID = deviceID
	svnReq.Code = code

	data, _ := json.Marshal(svnReq)

	r := bytes.NewReader(data)

	req, err := http.NewRequest(
		"POST",
		cfg.DriverAddress+cfg.DriverEndPoints["verification"],
		r,
	)
	if err != nil {
		fmt.Println(err)
		return obj.TokenResponse{}, err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return obj.TokenResponse{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst obj.TokenResponse
	decoder.Decode(&inst)

	logrus.Debugf("DriverID = [%v]", inst.DriverID)
	logrus.Debugf("DriverUUID = [%s]", inst.DriverUUID)
	logrus.Debugf("Token = [%s]", inst.Token)
	logrus.Debugf("RefreshToken = [%s]", inst.RefreshToken)

	return inst, nil
}

// DoRefreshJWTToken - (не работает)(http://192.168.0.103:6003/api/v2/auth/refresh)
func DoRefreshJWTToken(token string, refreshToken string) (obj.TokenResponse, error) {
	client := &http.Client{}

	type reqRefreshJWTToken struct {
		token string
	}
	var reqData reqRefreshJWTToken
	reqData.token = token

	data, _ := json.Marshal(reqData)
	logrus.Warn(string(data))
	r := bytes.NewReader(data)
	req, err := http.NewRequest(
		"POST",
		cfg.DriverAddress+cfg.DriverEndPoints["refresh"],
		r,
	)
	if err != nil {
		fmt.Println(err)
		return obj.TokenResponse{}, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return obj.TokenResponse{}, err
	}
	defer resp.Body.Close()

	io.Copy(os.Stdout, resp.Body)

	// decoder := json.NewDecoder(resp.Body)
	// var inst obj.TokenResponse
	// decoder.Decode(&inst)

	// logrus.Debugf("DriverID = [%v]", inst.DriverID)
	// logrus.Debugf("DriverUUID = [%s]", inst.DriverUUID)
	// logrus.Debugf("Token = [%s]", inst.Token)

	// return inst, nil

	return obj.TokenResponse{}, nil
}

// GetRabbitDataResp - структура ответ запроса DoGetRabbitData
type GetRabbitDataResp struct {
	DriveriD         int         `json:"driver_id"`
	DriverUUID       string      `json:"driver_uuid"`
	Connection       string      `json:"connection"`
	LocationExchange interface{} `json:"locations_exchange"`
	OrderConsumer    interface{} `json:"orders_consumer"`
}

// DoGetRabbitData - (http://192.168.0.103:6003/api/v2/brokerdata)
func DoGetRabbitData(token string) (GetRabbitDataResp, error) {
	client := &http.Client{}

	req, err := http.NewRequest(
		"POST",
		cfg.DriverAddress+cfg.DriverEndPoints["brokerdata"],
		nil,
	)
	if err != nil {
		fmt.Println(err)
		return GetRabbitDataResp{}, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return GetRabbitDataResp{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst GetRabbitDataResp
	decoder.Decode(&inst)

	// TODO переделать инициализацию кролика
	rab := myrabbit.New()
	err = rab.Init(inst.Connection)
	if err != nil {
		logrus.Fatalln(err)
	}

	// привязка к токену uuid водителя с помощью структуры
	var instance TokenUUID
	instance.Token = token
	instance.RabbitInst = *rab
	instance.DriverUUID = inst.DriverUUID
	instance.State = "offline"
	instance.LastLatitude = 0.0
	instance.LastLongitude = 0.0
	for i, val := range ListTokenUUID {
		if val.DriverUUID == instance.DriverUUID {
			ListTokenUUID = append(ListTokenUUID[:i], ListTokenUUID[i+1:]...)
		}
	}
	ListTokenUUID = append(ListTokenUUID, instance)

	logrus.Debugf("DriverID = [%v]", inst.DriveriD)
	logrus.Debugf("DriverUUID = [%s]", inst.DriverUUID)
	logrus.Debugf("Connection = [%s]", inst.Connection)
	logrus.Debugf("LocationExchange = [%s]", inst.LocationExchange)
	logrus.Debugf("OrderConsumer = [%s]", inst.OrderConsumer)

	return inst, nil
}

// RespDoSetDriverState - структура ответ запроса DoSetDriverState
type RespDoSetDriverState struct {
	Code    int    `json:"code"`
	Message string `json:"msg"`
}

// DoSetDriverState - TODO: сделать (http://192.168.0.103:6003/api/v2/setstate)
func DoSetDriverState(token string, state string) (RespDoSetDriverState, error) {
	client := &http.Client{}

	req, err := http.NewRequest(
		"POST",
		cfg.DriverAddress+cfg.DriverEndPoints["setstate"]+"?state="+state,
		nil,
	)
	if err != nil {
		fmt.Println(err)
		return RespDoSetDriverState{}, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return RespDoSetDriverState{}, err
	}
	defer resp.Body.Close()

	// установление статуса водителя в списке водителей (костыль)
	for i, val := range ListTokenUUID {
		if val.Token == token {
			ListTokenUUID[i].State = state
		}
	}

	//io.Copy(os.Stdout, resp.Body)

	decoder := json.NewDecoder(resp.Body)
	var inst RespDoSetDriverState
	decoder.Decode(&inst)

	logrus.Debugf("Code = [%v]", inst.Code)
	logrus.Debugf("Message = [%s]", inst.Message)

	return inst, nil
}

// DoSetOrderStatus - TODO: сделать (http://192.168.0.103:6003/api/v2/order)
func DoSetOrderStatus(token string) {

}

//GetMyOffersReq - структура запроса DoGetMyOffers
type GetMyOffersReq struct {
	State       string `json:"state"`
	OrderID     int    `json:"order_id"`
	TimeArrival int    `json:"time_arrival"`
	Message     string `json:"msg"`
}

//GetMyOffersResp - структура ответa DoGetMyOffers
type GetMyOffersResp struct {
	Offer     interface{} `json:"offer"`
	Order     interface{} `json:"order"`
	Service   interface{} `json:"service"`
	Owner     interface{} `json:"owner"`
	Client    interface{} `json:"client"`
	CreatedAt string      `json:"created_at"`
	Taximeter interface{} `json:"taximeter"`
}

// DoGetMyOffers - (http://192.168.0.103:6003/api/v2/myoffers)
func DoGetMyOffers(token string) (GetMyOffersResp, error) {
	client := &http.Client{}

	var gmoReq GetMyOffersReq
	gmoReq.State = "order_taken"
	gmoReq.OrderID = 35
	gmoReq.TimeArrival = 15
	gmoReq.Message = "this is how we do it..."

	data, _ := json.Marshal(gmoReq)

	r := bytes.NewReader(data)

	req, err := http.NewRequest(
		"GET",
		cfg.DriverAddress+cfg.DriverEndPoints["myoffers"],
		r,
	)
	if err != nil {
		fmt.Println(err)
		return GetMyOffersResp{}, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return GetMyOffersResp{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst GetMyOffersResp
	decoder.Decode(&inst)

	logrus.Debugf("Offer = [%v]", inst.Offer)
	logrus.Debugf("Order = [%s]", inst.Order)
	logrus.Debugf("Service = [%s]", inst.Service)
	logrus.Debugf("Owner = [%s]", inst.Owner)
	logrus.Debugf("Client = [%s]", inst.Client)
	logrus.Debugf("CreatedAt = [%s]", inst.CreatedAt)
	logrus.Debugf("Taximeter = [%s]", inst.Taximeter)
	return inst, nil
}

// PersonLoc - координаты пути водителя
type PersonLoc struct {
	DriverUUID string
	Latitude   float64
	Longitude  float64
}

// LocationList - набор координат пути каждого водителя
var LocationList []PersonLoc

// LocationData - отправка в брокер
type LocationData struct {
	tableName  struct{}  `sql:"drv_locations"`
	ID         int       `sql:",pk"`
	DriverID   int       `json:"driver_id"`
	DriverUUID string    `json:"driver_uuid"` //
	Latitude   float64   `json:"lat"`         //
	Longitude  float64   `json:"lng"`         //
	Satelites  int       `json:"sats"`
	Timestamp  int64     `json:"time"` //
	CreatedAt  time.Time `sql:"default:now()"`
}

// SendDriverLocation - горотина для отправки координат всех онлайн водителей
func SendDriverLocation(tick *time.Ticker) {
	var locData LocationData
	for range tick.C {
		for i, lt := range ListTokenUUID {
			if lt.State == "online" {
				locData.DriverUUID = lt.DriverUUID

				for j, ll := range LocationList {
					if ll.DriverUUID == lt.DriverUUID {
						ListTokenUUID[i].LastLatitude = ll.Latitude
						ListTokenUUID[i].LastLongitude = ll.Longitude
						LocationList = append(LocationList[:j], LocationList[j+1:]...)
						break
					}
				}
				locData.Latitude = ListTokenUUID[i].LastLatitude
				locData.Longitude = ListTokenUUID[i].LastLongitude
				locData.Timestamp = time.Now().Unix()
				logrus.Tracef("Driver: %v", locData.DriverUUID)
				logrus.Tracef("Latitude: %v", locData.Latitude)
				logrus.Tracef("Longitude: %v", locData.Longitude)

				data, _ := json.Marshal(locData)
				ListTokenUUID[i].RabbitInst.SendMsg(data)
			}
		}
		logrus.Tracef("Gorotine tick")
	}
}
