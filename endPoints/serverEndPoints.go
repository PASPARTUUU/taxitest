package endpoints

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	cfg "taxiTest/configs"
	obj "taxiTest/objects"

	"github.com/Sirupsen/logrus"
)

// ----------------------------------------------------------------------------------------------------------------------------

// DoDoLoginResponse - (http://192.168.0.103:6002/api/v2/login/) responsed when requesting token
func DoDoLoginResponse(login string, passwd string) (obj.LoginResponse, error) {
	client := &http.Client{}
	data := []byte(fmt.Sprintf(`{"login": "%s","password": "%s"}`, login, passwd))
	logrus.Warn(string(data))
	r := bytes.NewReader(data)
	req, err := http.NewRequest(
		"POST",
		cfg.ServerAddress+cfg.EndPoints["login"],
		r,
	)

	if err != nil {
		fmt.Println(err)
		return obj.LoginResponse{}, err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return obj.LoginResponse{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst obj.LoginResponse
	decoder.Decode(&inst)

	//oprArr = append(oprArr, inst)
	logrus.Debugf("ID = [%v]", inst.UserID)
	logrus.Debugf("Токен = [%s]", inst.Token)
	logrus.Debugf("Обновляемый токен = [%s]", inst.RefreshToken)
	logrus.Debugf("Срок годности токена = [%s]", inst.RefreshTokenExpiration)

	return inst, nil
}

// DoLoginResponse - (http://192.168.0.103:6002/api/v2/login/) responsed when requesting token
func DoLoginResponse(login string, passwd string) (obj.LoginResponse, error) {
	client := &http.Client{}
	data := []byte(fmt.Sprintf(`{"login": "%s","password": "%s"}`, login, passwd))
	logrus.Warn(string(data))
	r := bytes.NewReader(data)
	req, err := http.NewRequest(
		"POST",
		cfg.ServerAddress+cfg.EndPoints["login"],
		r,
	)

	if err != nil {
		fmt.Println(err)
		return obj.LoginResponse{}, err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return obj.LoginResponse{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst obj.LoginResponse
	decoder.Decode(&inst)

	//oprArr = append(oprArr, inst)
	logrus.Debugf("ID = [%v]", inst.UserID)
	logrus.Debugf("Токен = [%s]", inst.Token)
	logrus.Debugf("Обновляемый токен = [%s]", inst.RefreshToken)
	logrus.Debugf("Срок годности токена = [%s]", inst.RefreshTokenExpiration)

	return inst, nil
}

// DoGetOneRoute - (http://192.168.0.103:6002/api/v2/addresses)
func DoGetOneRoute(token string, addresses string) ([]obj.Route, error) {
	client := &http.Client{}
	data := []byte(fmt.Sprintf(`{"name": "%s"}`, addresses))
	r := bytes.NewReader(data)

	req, err := http.NewRequest(
		"POST",
		cfg.ServerAddress+cfg.EndPoints["addresses"],
		r,
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var routes []obj.Route
	decoder := json.NewDecoder(resp.Body)
	decoder.Decode(&routes)

	for i, inst := range routes {

		logrus.Debug("Address[%v] = [%s]", i, inst.UnrestrictedValue)
	}

	return routes, nil
}

// DoGetAllFeatures - (http://192.168.0.103:6002/api/v2/features)
func DoGetAllFeatures(token string) ([]obj.Feature, error) {
	client := &http.Client{}

	req, err := http.NewRequest(
		"GET",
		cfg.ServerAddress+cfg.EndPoints["features"],
		nil,
	)
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		//fmt.Println(err)
		return nil, err
	}
	defer resp.Body.Close()

	var allFeatures []obj.Feature
	//io.Copy(os.Stdout, resp.Body)
	decoder := json.NewDecoder(resp.Body)
	decoder.Decode(&allFeatures)

	for i, inst := range allFeatures {

		logrus.Debug("Name[%v] = [%s]", i, inst.Name)
	}

	return allFeatures, nil
}

// DoGetAllServices - (http://192.168.0.103:6002/api/v2/services)
func DoGetAllServices(token string) ([]obj.Service, error) {
	client := &http.Client{}

	req, err := http.NewRequest(
		"GET",
		cfg.ServerAddress+cfg.EndPoints["services"],
		nil,
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	var AllService []obj.Service
	//io.Copy(os.Stdout, resp.Body)
	decoder := json.NewDecoder(resp.Body)
	decoder.Decode(&AllService)

	for i, inst := range AllService {

		logrus.Debugf("Name[%v] = [%s]", i, inst.Name)
	}

	return AllService, nil
}

// DoGetOwnersList - (http://192.168.0.103:6002/api/v2/owners)
func DoGetOwnersList(token string) ([]obj.Owner, error) {
	client := &http.Client{}

	req, err := http.NewRequest(
		"GET",
		cfg.ServerAddress+cfg.EndPoints["owners"],
		nil,
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var allOwners []obj.Owner
	decoder := json.NewDecoder(resp.Body)
	decoder.Decode(&allOwners)

	for i, inst := range allOwners {

		logrus.Debugf("Name[%v] = [%s]", i, inst.Name)
	}

	return allOwners, nil
}

// DoGetAllCustomers -(http://192.168.0.103:6002/api/v2/clients)
func DoGetAllCustomers(token string) ([]obj.Client, error) {
	client := &http.Client{}

	req, err := http.NewRequest(
		"GET",
		cfg.ServerAddress+cfg.EndPoints["clients"],
		nil,
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	//io.Copy(os.Stdout, resp.Body)

	var allClients []obj.Client
	decoder := json.NewDecoder(resp.Body)
	decoder.Decode(&allClients)

	for i, inst := range allClients {

		logrus.Debugf("Name[%v] = [%s]", i, inst.Name)
	}

	return allClients, nil
}

// DoCreateOrder - (http://192.168.0.103:6002/api/v2/orders)
func DoCreateOrder(token string, oreder obj.CreaterOrder) (obj.OrderCRM, error) {
	client := &http.Client{}
	data, _ := json.Marshal(oreder)
	//fmt.Println(string(data))

	r := bytes.NewReader(data)
	req, err := http.NewRequest(
		"POST",
		cfg.ServerAddress+cfg.EndPoints["orders"],
		r,
	)
	if err != nil {
		fmt.Println(err)
		return obj.OrderCRM{}, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return obj.OrderCRM{}, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst obj.OrderCRM
	decoder.Decode(&inst)

	logrus.Debugf("UUID = [%v]", inst.UUID)
	logrus.Debugf("Клиент = [%s]", inst.Client.Name)
	logrus.Debugf("Тариф = [%s]", inst.Tariff.Name)

	return inst, nil
}

//-----------------------------------------------------------------------------------------------------------------------------

// DoRoute - (не сделанно) TODO точка назначения в заказе
func DoRoute(token string, addr string) {
	client := &http.Client{}
	data := []byte(fmt.Sprintf(`{"name": "%s"}`, addr))
	r := bytes.NewReader(data)

	req, err := http.NewRequest(
		"POST",
		"http://192.168.0.103:6002/api/v2/addresses",
		r,
	)
	if err != nil {
		fmt.Println(err)
		return
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst []obj.Route

	// err = json.Unmarshal([]byte(resp.Body), &inst)
	// if err != nil {
	// 	return
	// }
	err = decoder.Decode(&inst)
	if err != nil {
		logrus.Printf("Pizda  [%s]", err)
		return
	}

	logrus.Printf("UnrestrictedValue = [%s]", inst[0].UnrestrictedValue)
	// logrus.Printf("Value = [%s]", inst.Value)
	// logrus.Printf("Data: {")
	// var inst2 = inst.Data
	// logrus.Printf("	City = [%s]", inst2.City)
	// logrus.Printf("}")
}

// DoPostOwner - order owner structure (добавить таксопарк)
func DoPostOwner(token string, name string) {
	client := &http.Client{}
	data := []byte(fmt.Sprintf(`{"name": "%s"}`, name))
	fmt.Println(string(data))
	r := bytes.NewReader(data)
	req, err := http.NewRequest(
		"POST",
		"http://192.168.0.103:6002/api/v2/owners",
		r,
	)
	if err != nil {
		fmt.Println(err)
		return
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	var inst obj.Owner
	decoder.Decode(&inst)

	logrus.Printf("ID = [%v]", inst.UUID)
	logrus.Printf("Название = [%s]", inst.Name)
	logrus.Printf("Описание = [%s]", inst.Comment)
}

// GetOrders - (не сделанно) TODO
func GetOrders() {
	client := &http.Client{}

	req, err := http.NewRequest(
		"GET", "http://192.168.0.103:7002/api/v2/orders", nil,
	)
	if err != nil {
		fmt.Println(err)
		return
	}

	req.Header.Add("Content-Type", "application/json")
	//req.Header.Add("Authorization", "Bearer ")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()

	io.Copy(os.Stdout, resp.Body)
}

//-----------------------------------------------------------------------------------------------------------------------------

// DoSetOrderDeleted - удаление заказа по id (http://192.168.0.103:6002/api/v2/orders/8ec72589-0ad2-4f05-b8d5-b242d9e1b83e)
func DoSetOrderDeleted(token string, uuid string) (string, error) {
	client := &http.Client{}

	req, err := http.NewRequest(
		"DELETE",
		cfg.ServerAddress+cfg.EndPoints["orders"]+"/"+uuid,
		nil,
	)
	if err != nil {
		return "nil", err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		return "nil", err
	}
	defer resp.Body.Close()

	// io.Copy(os.Stdout, resp.Body)

	var message string
	decoder := json.NewDecoder(resp.Body)
	decoder.Decode(&message)

	return message, nil
}

//-----------------------------------------------------------------------------------------------------------------------------

// DoGetAllDrivers - (http://192.168.0.103:6002/api/v2/drivers)
func DoGetAllDrivers(token string) ([]obj.Driver, error) {
	client := &http.Client{}

	req, err := http.NewRequest(
		"GET",
		cfg.ServerAddress+cfg.EndPoints["drivers"],
		nil,
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var allDrivers []obj.Driver
	decoder := json.NewDecoder(resp.Body)
	decoder.Decode(&allDrivers)

	for i, inst := range allDrivers {

		logrus.Debugf("Name[%v] = [%s]", i, inst.Name)
	}

	return allDrivers, nil
}
