package script

import (
	cfg "taxiTest/configs"
	end "taxiTest/endPoints"
	obj "taxiTest/objects"
	"time"

	// "taxiTest/configs"

	"github.com/Sirupsen/logrus"
)

// SuperOperator - значения супер ператора
var SuperOperator obj.LoginResponse

// IniVal - общие данные из базы
type IniVal struct {
	// LoginResp - данные при входе оператора
	LoginResps []obj.LoginResponse
	// Routes - массив всех роутов с автокомплита
	Routes []obj.Route
	// Features - массив всех фич из базы
	Features []obj.Feature
	// Services - массив всех сервисов из базы
	Services []obj.Service
	// Owners - массив всех таксопарков из базы
	Owners []obj.Owner
	// Clients - массив всех клиентов из базы
	Clients []obj.Client
}

// Initial - считывает с базы инфу в структуру IniVal
func Initial() IniVal {
	var err error

	// получаем токен супер оператора
	logrus.Info("Создаю супер оператора")
	LoginResp, err := end.DoLoginResponse("admin", "111111")
	if err != nil {
		logrus.Errorf("ошибка инициализации оператора, %s", err)
	}
	SuperOperator = LoginResp

	var iniVal IniVal

	logrus.Info("Получен список операторов")
	admins := [][2]string{
		// {"admin", "111111"},
		// {"admin", "111111"},
		// {"admin", "111111"},
	}
	var LoginResps []obj.LoginResponse
	for i := 0; i < len(admins); i++ {
		LoginResp, err := end.DoLoginResponse(admins[i][0], admins[i][1])
		if err != nil {
			logrus.Errorf("ошибка инициализации операторов, %s", err)
		}
		LoginResps = append(LoginResps, LoginResp)
	}
	iniVal.LoginResps = LoginResps

	//fmt.Println("Создаю таксопарк")
	//DoOwner(LoginResp.Token, "Testuser195")

	//fmt.Println("Создаю доп. функцию")
	//DoFeature(LoginResp.Token, "Массажное сиденье", "Делает массаж",

	// DoGetRoutes
	logrus.Info("Получены все роуты")
	Routes, err := end.DoGetOneRoute(LoginResp.Token, "фыва")
	if err != nil {
		logrus.Errorf("ошибка инициализации роутов, %s", err)
	}
	iniVal.Routes = Routes
	// DoGetFeatures
	logrus.Info("Получены все фичи")
	Features, err := end.DoGetAllFeatures(LoginResp.Token)
	if err != nil {
		logrus.Errorf("ошибка инициализации фичей, %s", err)
	}
	iniVal.Features = Features
	// DoGetService
	logrus.Info("Получены все сервисы")
	Services, err := end.DoGetAllServices(LoginResp.Token)
	if err != nil {
		logrus.Errorf("ошибка инициализации сервисов, %s", err)
	}
	iniVal.Services = Services
	// DoGetOwner
	logrus.Info("Получены все таксопарки")
	Owners, err := end.DoGetOwnersList(LoginResp.Token)
	if err != nil {
		logrus.Errorf("ошибка инициализации таксопарков, %s", err)
	}
	iniVal.Owners = Owners
	// DoGetClient
	logrus.Info("Получены всe клиенты")
	Clients, err := end.DoGetAllCustomers(LoginResp.Token)
	if err != nil {
		logrus.Errorf("ошибка инициализации клиентов, %s", err)
	}
	iniVal.Clients = Clients

	// запуск горотины - отправка координат
	dur, _ := time.ParseDuration(cfg.DriverLocationPushPeriod)
	ticker := time.NewTicker(dur)
	go end.SendDriverLocation(ticker)

	return iniVal
}
