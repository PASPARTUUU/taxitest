package script

import (
	"math/rand"
	"time"
)

// RandInt - возвращает рандомное число от 0 до val
func RandInt(val int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(val)
}

// RandIntFromRange - возвращает рандомное число от min до max (0,1)=0
func RandIntFromRange(min int, max int) int {
	//max++  randInt(0,0) = 0
	rand.Seed(time.Now().UTC().UnixNano())
	return min + rand.Intn(max-min)
}
