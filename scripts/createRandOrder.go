package script

import (
	"errors"
	end "taxiTest/endPoints"
	obj "taxiTest/objects"

	"github.com/Sirupsen/logrus"
)

// CreateRandOrder - содание случайного заказа TODO: исправить роуты
func CreateRandOrder(iniVal IniVal, featureQuantity int, routeQuantity int) (obj.OrderCRM, error) {
	var newOrder obj.CreaterOrder
	var r, l int

	//
	l = len(iniVal.LoginResps)
	if l == 0 {
		return obj.OrderCRM{}, errors.New("Пустой список операторов")
	}
	r = RandInt(l)
	logrus.Debugf("оператор - %v\n", r)
	operator := iniVal.LoginResps[r]

	//
	l = len(iniVal.Clients)
	if l == 0 {
		return obj.OrderCRM{}, errors.New("Пустой список клиентов")
	}
	r = RandInt(l)
	logrus.Debugf("клиент - %v\n", r)
	newOrder.Client = iniVal.Clients[r]

	//
	newOrder.CallbackPhone = "123456"
	newOrder.Comment = "урановый двигатель"

	//
	l = len(iniVal.Features)
	if l == 0 {
		return obj.OrderCRM{}, errors.New("Пустой список фич")
	}
	var newMassFeatures []obj.Feature
	for i := 0; i < featureQuantity; i++ {
		r = RandInt(l)
		logrus.Debugf("фича - %v\n", r)
		newMassFeatures = append(newMassFeatures, iniVal.Features[r])
	}
	newOrder.Features = newMassFeatures

	//
	l = len(iniVal.Routes)
	if l == 0 {
		return obj.OrderCRM{}, errors.New("Пустой список роутов")
	}
	var newMassRoute []obj.Route
	for i := 0; i < routeQuantity; i++ {
		r = RandInt(l)
		logrus.Debugf("роут - %v\n", r)
		newMassRoute = append(newMassRoute, iniVal.Routes[r])
	}
	newOrder.Routes = newMassRoute

	//
	l = len(iniVal.Owners)
	if l == 0 {
		return obj.OrderCRM{}, errors.New("Пустой список таксопарков")
	}
	r = RandInt(l)
	logrus.Debugf("таксопарк - %v\n", r)
	newOrder.OwnerUUID = iniVal.Owners[r].UUID

	//
	l = len(iniVal.Services)
	if l == 0 {
		return obj.OrderCRM{}, errors.New("Пустой список сервисов")
	}
	r = RandInt(l)
	logrus.Debugf("сервис - %v\n", r)
	newOrder.ServiceUUID = iniVal.Services[r].UUID

	logrus.Info("Новый заказ создан")
	return end.DoCreateOrder(operator.Token, newOrder)
}
