package script

import (
	"fmt"
	end "taxiTest/endPoints"

	"github.com/Sirupsen/logrus"
)

// DriverEmulation - (получение кода подтверждения, получение токена, получение кролика)
func DriverEmulation() error {
	var err error

	_, err = end.DoGetSMSVerificationNumber("83765hgs4", "+79998887766")
	if err != nil {
		logrus.Error(err)
		return err
	}
	fmt.Println("-------------------")
	param, err := end.DoSendVerificationNumber("83765hgs4", 1080)
	if err != nil {
		logrus.Error(err)
		return err
	}
	_, err = end.DoGetRabbitData(param.Token)
	if err != nil {
		logrus.Error(err)
		return err
	}

	// искусственный список перемешения
	var obb end.PersonLoc
	for i := 0; i < 10; i++ {
		obb.DriverUUID = param.DriverUUID
		obb.Latitude = 40 + float64(i) + 0.1
		obb.Longitude = 40 + float64(i) + 0.2
		end.LocationList = append(end.LocationList, obb)
	}

	// fmt.Println("-------------------")
	// _, err = end.DoRefreshJWTToken(param.Token, param.RefreshToken)
	// if err != nil {
	// 	logrus.Error(err)
	// }
	fmt.Println("-------------------")
	_, err = end.DoSetDriverState(param.Token, "online")
	if err != nil {
		logrus.Error(err)
		return err
	}
	fmt.Println("-------------------")

	//======================================================

	// _, err = end.DoGetSMSVerificationNumber("efewfwfef", "+7999888ййй7766")
	// if err != nil {
	// 	logrus.Error(err)
	// 	return err
	// }
	// fmt.Println("-------------------")
	// param2, err := end.DoSendVerificationNumber("efewfwfef", 1080)
	// if err != nil {
	// 	logrus.Error(err)
	// 	return err
	// }
	// fmt.Println("-------------------")
	// _, err = end.DoGetRabbitData(param.Token)
	// if err != nil {
	// 	logrus.Error(err)
	// 	return err
	// }

	// // // искусственный список перемешения
	// for i := 0; i < 10; i++ {
	// 	obb.DriverUUID = param2.DriverUUID
	// 	obb.Latitude = 50 + float64(i) + 0.1
	// 	obb.Longitude = 50 + float64(i) + 0.2
	// 	end.LocationList = append(end.LocationList, obb)
	// }
	// fmt.Println("-------------------")
	// _, err = end.DoSetDriverState(param2.Token, "online")
	// if err != nil {
	// 	logrus.Error(err)
	// 	return err
	// }

	// fmt.Println("-------------------")
	// _, err = end.DoGetMyOffers(param.Token)
	// if err != nil {
	// 	logrus.Error(err)
	// }

	return nil
}
