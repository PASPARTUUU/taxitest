package myrabbit

import (
	"github.com/streadway/amqp"
)

const (
	userURL     = "192.168.0.103:6004"
	userCredits = "barmen:kfclover97"
)

// MyRabbit - кроличья структура (соединение, канал, очередь)
type MyRabbit struct {
	Connection *amqp.Connection
	Channel    *amqp.Channel
	Queue      amqp.Queue
}

// New - создает Rabbit инстанс
func New() *MyRabbit {
	return &MyRabbit{}
}

// Init - Инииализация кролика (подключение к серверу, создание канала, обЪявление очереди)
func (rb *MyRabbit) Init(connect string) error {
	var err error

	// поключение к серверу
	// rb.Connection, err = amqp.Dial(fmt.Sprintf("amqp://%s@%s/", userCredits, userURL))
	rb.Connection, err = amqp.Dial(connect)
	if err != nil {
		return err
	}

	// создание канала
	rb.Channel, err = rb.Connection.Channel()
	if err != nil {
		return err
	}

	// объявление(создание) очереди для отправки
	rb.Queue, err = rb.Channel.QueueDeclare(
		"TestQueue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	return nil
}

// SendMsg - отправка сообщения в очередь
func (rb *MyRabbit) SendMsg(data []byte) error {
	err := rb.Channel.Publish(
		"",
		rb.Queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        data,
		},
	)
	return err
}
