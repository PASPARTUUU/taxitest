package myсonfig

const (
	// DriverLocationPushPeriod - интервал времени передачи координат
	DriverLocationPushPeriod = "1s"
)

// ServerAddress - адрес сервера
var ServerAddress = "http://192.168.0.103:7002/api/v2"

// EndPoints - cписок роутов
var EndPoints = map[string]string{
	"login":     "/login/",
	"addresses": "/addresses",
	"features":  "/features",
	"services":  "/services",
	"owners":    "/owners",
	"clients":   "/clients",
	"orders":    "/orders",

	"drivers": "/drivers",
}

// DriverAddress - сервер водителей
var DriverAddress = "http://192.168.0.103:6003/api/v2"

// DriverEndPoints - роуты водителей
var DriverEndPoints = map[string]string{
	"authorivation": "/auth/new",
	"verification":  "/auth/verification",
	"refresh":       "/auth/refresh",
	"brokerdata":    "/brokerdata",
	"setstate":      "/setstate", //"/setstate?state=online"
	"order":         "/order",
	"myoffers":      "/myoffers",
}
