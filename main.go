package main

import (
	"bufio"
	"fmt"
	"os"

	// cfg "taxiTest/configs"
	// end "taxiTest/endPoints"
	// scr "taxiTest/scripts"

	scr "taxiTest/scripts"

	"github.com/Sirupsen/logrus"
)

//io.Copy(os.Stdout, resp.Body)
func init() {
	//log.SetFormatter(&log.JSONFormatter{})
	logrus.SetOutput(os.Stdout)

	logrus.SetLevel(logrus.TraceLevel)
	//log.SetLevel(log.InfoLevel)
}

const (
	// basic
	opHelp  = "help"
	opClose = "close"

	// scripts
	opDriverEmulation = "driverem"
	opCreateRandOrder = "randorder"
	// server
	opDoLoginResponse = "logresp"
	// drivers
	opDoGetSMSVerificationNumber = "smsver"
	opDoSendVerificationNumber   = "sendver"
	opDoGetRabbitData            = "getrabb"
)

func operationList() {
	fmt.Println("===")
	fmt.Println("Скрипты:")
	fmt.Println("\t" + opCreateRandOrder + "(featureQuantity, routeQuantity)" + ":\t" + "создание случайного заказа")
	fmt.Println("\t" + opDriverEmulation + "()" + ":\t" + "эмуляция водителя")

	fmt.Println("---")
	fmt.Println("Серверные ендпоинты:")
	fmt.Println("\t" + opDoLoginResponse + "(login, passwd)" + ":\t" + "получение кода через смс")

	fmt.Println("---")
	fmt.Println("Водительские ендпоинты:")
	fmt.Println("\t" + opDoGetSMSVerificationNumber + "(device, phone)" + ":\t" + "получение кода через смс")
	fmt.Println("\t" + opDoSendVerificationNumber + "(device, code)" + ":\t" + "подтверждение кода")
	fmt.Println("\t" + opDoGetRabbitData + "(token)" + ":\t" + "получение кроличьих данных")

	fmt.Println("---")

	fmt.Println("Системные:")
	fmt.Println("\t" + opHelp + ":\t" + "список команд")
	fmt.Println("\t" + opClose + ":\t" + "завершение работы")

	fmt.Println("===")
}

func main() {
	fmt.Println("i belive i can fly")
	// end.DoDoLoginResponse("admin", "111111")

	// IniData := scr.Initial()
	scr.Initial()
	// var err error

	// var rb myrabbit.MyRabbit
	// rb.Init()
	// if err != nil {
	// 	logrus.Fatalln(err)
	// }

	// rb.SendMsg([]byte("i can fly"))
	// if err != nil {
	// 	logrus.Fatalln(err)
	// }

	// //var close = false
	// var close = false
	// for close != true {
	// 	fmt.Println("||||||||||||||||")
	// 	fmt.Print("Выбoр операции: ")
	// 	words := mySplitter(consoleRead())

	// 	switch words[0] {
	// 	// scripts:

	// 	// создание случайного заказа
	// 	case opCreateRandOrder:
	// 		if len(words) < 3 {
	// 			logrus.Errorln("Не указанны операторы")
	// 			break
	// 		}
	// 		nmb1, err := strconv.ParseInt(words[1], 10, 64)
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 			break
	// 		}
	// 		nmb2, err := strconv.ParseInt(words[2], 10, 64)
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 			break
	// 		}
	// 		_, err = scr.CreateRandOrder(IniData, int(nmb1), int(nmb2))
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 		}
	// 	case opDriverEmulation:
	// 		err = scr.DriverEmulation()
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 		}

	// 	// server
	// 	case opDoLoginResponse:
	// 		err = end.DoLoginResponse()
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 		}

	// 	// drivers:
	// 	case opDoGetSMSVerificationNumber:
	// 		if len(words) < 3 {
	// 			logrus.Errorln("Не указанны операторы")
	// 			break
	// 		}
	// 		_, err = end.DoGetSMSVerificationNumber(words[1], words[2])
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 		}

	// 	case opDoSendVerificationNumber:
	// 		if len(words) < 3 {
	// 			logrus.Errorln("Не указанны операторы")
	// 			break
	// 		}
	// 		nmb, err := strconv.ParseInt(words[2], 10, 64)
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 			break
	// 		}
	// 		_, err = end.DoSendVerificationNumber(words[1], int(nmb))
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 		}

	// 	case opDoGetRabbitData:
	// 		if len(words) < 2 {
	// 			logrus.Errorln("Не указанны операторы")
	// 			break
	// 		}

	// 		_, err = end.DoGetRabbitData(words[1])
	// 		if err != nil {
	// 			logrus.Errorln(err)
	// 		}

	// 	// system:
	// 	case opHelp:
	// 		operationList()

	// 	case opClose:
	// 		close = true

	// 	default:
	// 		fmt.Println("Missing")
	// 	}
	// }

	scr.DriverEmulation()

	fmt.Scanln()
	//script.DriverEmulation()

}

func consoleRead() string {
	in := bufio.NewReader(os.Stdin)
	str, err := in.ReadString('\n')
	if err != nil {
		fmt.Println("Ошибка ввода: ", err)
	}
	return string(str[:len(str)-1])
}

func mySplitter(str string) []string {
	var split []string
	str = str + " "
	n := len(str)

	var ind1 int
	var ind2 int

	for i := 0; i < n; i++ {
		if string(str[i]) == " " {
			ind2 = i
			if str[ind1:ind2] != " " && ind1 != ind2 {
				split = append(split, str[ind1:ind2])
			}
			ind1 = i + 1
		}
	}
	return split
}
